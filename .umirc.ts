import { defineConfig } from '@umijs/max';
import theme, { components, token } from 'iglootheme';

export default defineConfig({
  antd: {
    theme: {
      token: {
        ...token,
        colorLink: token.colorPrimary,
        colorLinkHover: '#8286ff',
        colorLinkActive: '#4641d9',
      },
      components,
    },
  },
  layout: false,
  model: {},
  initialState: {},
  request: {},
  lessLoader: {
    modifyVars: theme,
  },
  locale: {
    default: 'zh-CN',
  },
  npmClient: 'pnpm',
  tailwindcss: {},
  icons: {},
  mountElementId: process.env.PROJECT_NAME,
});
