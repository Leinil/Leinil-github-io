import { Spin } from 'antd';

const Loading = () => {
  return (
    <div style={{ display: 'flex', height: 'calc(100vh - 112px)' }}>
      <Spin style={{ margin: 'auto' }} />
    </div>
  );
};

export default Loading;
