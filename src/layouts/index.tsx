import { Outlet } from 'umi';
import {Typography} from 'iglooform';

export default function Page() {
  return (
    <div style={{background:'#F6F6FF'}}>
      <div className={'py-2 w-full flex items-center justify-center'} style={{boxShadow: "0px 1px 4px 0px #00000029"}}>
        <Typography level={'h2'} style={{color:'#666666'}}>四川省MM数智小工具-1 分期</Typography>
      </div>
      <div style={{ padding: 20 }} className="flex justify-center bg-white">
        <div className="max-w-[800px] ">
          <Outlet />
        </div>
      </div>
    </div>
  );
}
