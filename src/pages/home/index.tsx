import { Button, Typography } from 'iglooform';
import {history} from '@umijs/max';

const HomePage = () => {
  return (
    <>
      <div className="text-center">
        <Typography level="h2"></Typography>
      </div>
      <div className="my-5">
        <div>
          <Typography level={'body2'}>请您准备如下
            <Typography level={'body1'} style={{color:'red'}} className={'font-black'}>七</Typography>
            个检查报告：</Typography>
        </div>
        <Typography level="body2">
          血常规、尿常规、生化报告、X线/PET/MRI任一、特殊蛋白检验报告、β2微球蛋白、荧光原位杂交(FISH)结合您的MM检查报告回答以下九个问题以进行分期计算。
        </Typography>
      </div>
      <div className="text-center">
        <Button onClick={()=>history.push('/questions')} type='primary'>开始评估</Button>
      </div>
    </>
  );
};

export default HomePage;
