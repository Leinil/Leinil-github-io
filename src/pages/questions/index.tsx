import BreadCrumb from '@/components/bread-crumb';
import { history } from '@umijs/max';
import cls from 'classnames';
import {
  Button,
  CheckboxGroup,
  FormItem,
  FormSection,
  FreeForm,
  Radio,
  RadioGroup,
} from 'iglooform';
import { useEffect, useMemo, useState } from 'react';

const Question9 = ({ value, onChange }: any) => {
  const [checked, setChecked] = useState<string[]>([]);
  console.log(value);
  useEffect(() => {
    if (value) {
      if (value && value.includes('e')) {
        setChecked(['e']);
        onChange(['e']);
      } else {
        setChecked(value);
      }
    }
  }, [JSON.stringify(value)]);

  const options = useMemo(() => {
    const finalValue = value || [];
    return [
      {
        label: 'a.del（17p)',
        value: 'a',
      },
      {
        label: 'b.t(4,14)',
        value: 'b',
      },
      {
        label: 'c.1q+',
        value: 'c',
      },
      {
        label: 'd.t(14,16)',
        value: 'd',
      },
      {
        label: 'e.以上条件都不满足',
        value: 'e',
      },
    ].map((o) => ({
      ...o,
      label: finalValue.includes(o.value) ? o.label + ' ' : o.label + '  ',
      disabled: value && value.includes('e') && o.value !== 'e',
    }));
  }, [JSON.stringify(value)]);

  return (
    <CheckboxGroup
      className="ml-6 mt-2"
      value={checked}
      onChange={onChange}
      options={options}
    ></CheckboxGroup>
  );
};

const FinalQuestion = ({ onChange, value }: any) => {
  return (
    <>
      <div className="flex flex-col justify-center">
        <Radio value={'9A'} checked={value === '9A'} onChange={onChange}>
          A. 已做该检查
        </Radio>
        <FormItem
          name="ans9A"
          shouldRender={() => value === '9A'}
          className="ml-6 mt-4"
        >
          <Question9 />
        </FormItem>
        <Radio
          value={'9B'}
          checked={value === '9B'}
          onChange={onChange}
          className="mt-4"
        >
          B. 未做该检查
        </Radio>
      </div>
    </>
  );
};

const Q5 = ({ onChange, value }: any) => {
  return (
    <div className="flex flex-col">
      <Radio value={'5A'} checked={value === '5A'} onChange={onChange}>
        a.免疫球蛋白g（IgG）
      </Radio>
      <FormItem
        label=""
        name="ans5A"
        className="ml-6 mt-4"
        shouldRender={() => value === '5A'}
      >
        <RadioGroup
          options={[
            {
              label: 'a.IgG<50g/L',
              value: '5Aa',
            },
            {
              label: 'b.IgG>70g/L',
              value: '5Ab',
            },
            {
              label: 'c.以上均不符合',
              value: '5Ac',
            },
            {
              label: 'd.未做该检查',
              value: '5Ad',
            },
          ]}
        ></RadioGroup>
      </FormItem>
      <Radio
        value={'5B'}
        checked={value === '5B'}
        onChange={onChange}
        className="mt-4"
      >
        b.免疫球蛋白g（IgA）
      </Radio>
      <FormItem
        label=""
        name="ans5B"
        className="ml-6 mt-4"
        shouldRender={() => value === '5B'}
      >
        <RadioGroup
          options={[
            {
              label: 'a.IgA<30g/L  ',
              value: '5Ba',
            },
            {
              label: 'b.IgA>50g/L',
              value: '5Bb',
            },
            {
              label: 'c.以上均不符合',
              value: '5Bc',
            },
            {
              label: 'd.未做该检查',
              value: '5Bd',
            },
          ]}
        ></RadioGroup>
      </FormItem>
    </div>
  );
};

// 那些得分问题的具体分数
const ansWithScore: Record<string, number> = {
  '7B': 1.5,
  '7C': 1,
  '8B': 1,
  ans9Aa: 1,
  ans9Ab: 1,
  ans9Ac: 0.5,
};

function getScope(qNum: number, userAns: any) {
  // 只会在第九题选择9a的时候执行
  if (qNum === 9) {
    // 是个数组
    const ans9A = userAns['ans9A'] as string[];
    const total = ans9A.reduce((res, cur) => {
      return res + (ansWithScore[`ans9A${cur}`] || 0);
    }, 0);
    return total;
  }

  return ansWithScore[userAns[`q${qNum}`]] || 0;
}

const Questions = () => {
  // 9个问题分三组
  const [group, setGroup] = useState(1);
  const [form] = FreeForm.useForm();

  const handleGotoNextGroup = async (groupIndex: number) => {
    const validateMap: Record<string, any> = {
      1: ['q1', 'q2', 'q3'],
      2: ['q4', 'q5', 'q6', 'ans5A', 'ans5B'],
      3: ['q7', 'q8', 'q9', 'ans9A'],
    };
    await form.validateFields(validateMap[groupIndex]);
    setGroup(groupIndex + 1);
  };

  const handleFinalCheck = async () => {
    await form.validateFields();
    const value = form.getFieldsValue();
    console.log(value);
    let dsType = '';
    let kind = '';
    let ISSType = '';
    let RISSType = '';
    let scope = 0;
    let R2ISSType = '';

    if (
      value['q1'] == '1D' &&
      value['q2'] === '2C' &&
      value['q4'] === '4D' &&
      (value['ans5A'] === '5Ad' || value['ans5B'] === '5Bd') &&
      value['q6'] === '6D'
    ) {
      dsType = 'no result';
    } else if (
      value['q1'] == '1A' &&
      value['q2'] === '2A' &&
      value['q4'] === '4A' &&
      (value['ans5A'] === '5Aa' || value['ans5B'] === '5Ba') &&
      value['q6'] === '6A'
    ) {
      dsType = 'I';
    } else if (
      value['q1'] == '1B' ||
      value['q2'] === '2B' ||
      value['q4'] === '4B' ||
      value['ans5A'] === '5Ab' ||
      value['ans5B'] === '5Bb' ||
      value['q6'] === '6A'
    ) {
      dsType = 'III';
    } else {
      dsType = 'II';
    }

    if (value['q3'] === '3A') {
      kind = 'A';
    }
    if (value['q3'] === '3B') {
      kind = 'B';
    }

    switch (value['q7']) {
      case '7A':
        ISSType = 'I';
        break;
      case '7B':
        ISSType = 'III';
        break;
      case '7C':
        ISSType = 'II';
        break;
    }

    if (
      value['q7'] === '7A' &&
      value['q8'] === '8A' &&
      value['ans9A'] &&
      value['ans9A'].length === 1 &&
      value['ans9A'][0] === 'e'
    ) {
      RISSType = 'I';
    } else if (
      value['q7'] === '7B' ||
      value['q8'] === '8B' ||
      (value['ans9A'] &&
        value['ans9A'].some((a: string) => ['a', 'b', 'd'].includes(a)))
    ) {
      RISSType = 'III';
    } else if (value['q7'] === '7D' && value['q8'] === '8C') {
      RISSType = 'no result';
    } else if (value['q9'] === '9B') {
      RISSType = '';
    } else {
      RISSType = 'II';
    }

    // 也就是做了检查
    if (value['q9'] !== '9B') {
      scope = getScope(7, value) + getScope(8, value) + getScope(9, value);

      if (scope === 0) {
        R2ISSType = 'I';
      }

      if (scope >= 0.5 && scope <= 1) {
        R2ISSType = 'II';
      }

      if (scope >= 1.5 && scope <= 2.5) {
        R2ISSType = 'III';
      }

      if (scope >= 3 && scope <= 5) {
        R2ISSType = 'IV';
      }
    } else {
      scope = -1;
    }

    sessionStorage.setItem(
      'res',
      JSON.stringify({
        dsType,
        kind,
        ISSType,
        RISSType,
        R2ISSType,
        scope,
      }),
    );

    history.push('/result');
  };

  return (
    <>
      <BreadCrumb
        items={[
          {
            title: '首页',
            onClick: () => history.push('/home'),
          },
          {
            title: '问题',
          },
        ]}
      />
      <FreeForm
        form={form}
        locales={{
          validateMessages: {
            required: '该选项必选',
          },
        }}
      >
        <div className={cls(group !== 1 ? 'hidden' : '')}>
          <FormSection>
            <FormItem label="一、血常规" name="q1" fullRow>
              <RadioGroup
                options={[
                  {
                    label: 'a.血红蛋白水平 > 100 g/L',
                    value: '1A',
                  },
                  {
                    label: 'b.血红蛋白水平 < 85 g/L',
                    value: '1B',
                  },
                  {
                    label: 'c.以上均不符合',
                    value: '1C',
                  },
                  {
                    label: 'd.未做该检查',
                    value: '1D',
                  },
                ]}
              ></RadioGroup>
            </FormItem>

            <FormItem label="二、生化报告" name="q2" fullRow>
              <RadioGroup
                options={[
                  {
                    label: 'a.血清钙(Ca) ≤ 2.65 mmol/L (11.5mg/dL)',
                    value: '2A',
                  },
                  {
                    label: 'b.血清钙(Ca) > 2.65 mmol/L (11.5mg/dL)',
                    value: '2B',
                  },
                  {
                    label: 'c.未做该检查',
                    value: '2C',
                  },
                ]}
              ></RadioGroup>
            </FormItem>

            <FormItem label="三、生化报告肾功" name="q3" fullRow>
              <RadioGroup
                options={[
                  {
                    label:
                      'a.肌酐清除率>40 mL/min或血清肌酐水平<177umo/L (2.0 mg/dL)',
                    value: '3A',
                  },
                  {
                    label:
                      'b.肌酐清除率≤40 mL/min或血清肌酐水平≥177umol/L (2.0 mg/dL)',
                    value: '3B',
                  },
                  {
                    label: 'c.未做该检查',
                    value: '3C',
                  },
                ]}
              ></RadioGroup>
            </FormItem>
          </FormSection>
        </div>

        <div className={cls(group !== 2 ? 'hidden' : '')}>
          <FormSection>
            <FormItem label="四、X线、PET、MRI" name="q4" fullRow>
              <RadioGroup
                options={[
                  {
                    label: 'a.骨骼X线：骨骼结构正常或骨型孤立性浆细胞瘤',
                    value: '4A',
                  },
                  {
                    label: 'b.骨骼检查中溶骨病变>3处',
                    value: '4B',
                  },
                  {
                    label: 'c.以上均不符合',
                    value: '4C',
                  },
                  {
                    label: 'd.未做该检查',
                    value: '4D',
                  },
                ]}
              ></RadioGroup>
            </FormItem>

            <FormItem label="五、特殊蛋白检验报告" name="q5" fullRow>
              <Q5 />
            </FormItem>

            <FormItem label="六、尿常规" name="q6" fullRow>
              <RadioGroup
                options={[
                  {
                    label: 'a.尿本周蛋白Bence Jones<4g/24h',
                    value: '6A',
                  },
                  {
                    label: 'b.尿本周蛋白Bence Jones>12g/24h',
                    value: '6B',
                  },
                  {
                    label: 'c.以上均不符合',
                    value: '6C',
                  },
                  {
                    label: 'd.未做该检查',
                    value: '6D',
                  },
                ]}
              ></RadioGroup>
            </FormItem>
          </FormSection>
        </div>

        <div className={cls(group !== 3 ? 'hidden' : '')}>
          <FormSection>
            <FormItem label="七、β2微球蛋白、生化报告" name="q7" fullRow>
              <RadioGroup
                options={[
                  {
                    label: 'a.β2-MG<3.5mg/L且白蛋白≥35g/L',
                    value: '7A',
                  },
                  {
                    label: 'b.β2-MG≥5.5mg/L',
                    value: '7B',
                  },
                  {
                    label: 'c.以上条件都不满足',
                    value: '7C',
                  },
                  {
                    label: 'd.未做该检查',
                    value: '7D',
                  },
                ]}
              ></RadioGroup>
            </FormItem>

            <FormItem label="八、生化报告" name="q8" fullRow>
              <RadioGroup
                options={[
                  {
                    label: 'a.乳酸脱氢酶（LDH）箭头朝下',
                    value: '8A',
                  },
                  {
                    label: 'b.乳酸脱氢酶（LDH）箭头朝上',
                    value: '8B',
                  },
                  {
                    label: 'c.未做该检查',
                    value: '8C',
                  },
                ]}
              ></RadioGroup>
            </FormItem>

            <FormItem label="九、荧光原位杂交（FISH）" name="q9" fullRow>
              <FinalQuestion />
            </FormItem>
          </FormSection>
        </div>
      </FreeForm>

      <div className="flex justify-between mt-5">
        <div>
          <Button
            onClick={() => setGroup((group) => group - 1)}
            className={cls(group === 1 ? 'hidden' : '')}
          >
            上一组
          </Button>
        </div>
        <div className={cls(group === 3 ? 'hidden' : '')}>
          <Button onClick={() => handleGotoNextGroup(group)}>下一组</Button>
        </div>
        <div
          className={cls(group !== 3 ? 'hidden' : '')}
          onClick={handleFinalCheck}
        >
          <Button type="primary">查看结果</Button>
        </div>
      </div>
    </>
  );
};

export default Questions;
