import { history } from '@umijs/max';
import { Col, Image, Row } from 'antd';
import { Button, Documents, Typography } from 'iglooform';
import 'react-pdf/dist/esm/Page/AnnotationLayer.css';
import 'react-pdf/dist/esm/Page/TextLayer.css';

import Reference from '@/assets/reference.pdf';
import BreadCrumb from '@/components/bread-crumb';
import pic1 from '@/icons/pic1.png';
import pic2 from '@/icons/pic2.png';
import tableIcon from '@/icons/table.jpg';

import styles from './index.less';

const Result = () => {
  const sessionRes = sessionStorage.getItem('res') || '{}';
  const jsonRes = JSON.parse(sessionRes);
  const { dsType, kind, ISSType, RISSType, R2ISSType, scope } = jsonRes;
  let resultContent = null;

  if (dsType == 'no result' || RISSType == 'no result') {
    resultContent = (
      <div>
        <Typography level={'h3a'}>
          您尚未提供关键数据，请先完成相关检查
        </Typography>
      </div>
    );
  } else {
    resultContent = (
      <>
        <div>
          <Typography level="h2">结果显示</Typography>
        </div>
        <div>
          {scope !== -1 && (
            <Typography level="h3a">
              评分
              <Typography level="h3a" className={styles.scope}>
                {scope}
              </Typography>
            </Typography>
          )}
        </div>
        <div>
          <Typography level="h3a">
            <span>DS</span>
            <span className="text-amber-200"> {dsType}</span>期
            {kind && (
              <>
                <span className="text-amber-200"> {kind}</span>
                <span>亚型</span>
              </>
            )}
          </Typography>
        </div>
        <div>
          <Typography level="h3a">
            {ISSType && (
              <>
                <span>ISS</span>
                <span className="text-amber-200"> {ISSType}</span>期
              </>
            )}
            <span> R-ISS</span>
            <span className="text-amber-200"> {RISSType}</span>期
          </Typography>

          {scope !== -1 && (
            <Typography level="h3a" className="ml-2">
              <span>R2-ISS</span>
              <span className="text-amber-200"> {R2ISSType}</span>期
            </Typography>
          )}
        </div>
      </>
    );
  }

  return (
    <>
      <BreadCrumb
        items={[
          {
            title: '首页',
            onClick: () => history.push('/home'),
          },
          {
            title: '测试结果',
          },
        ]}
      />
      <Row gutter={32} wrap className={'mt-4'}>
        <Col span={24}>
          <div className="rounded-xl bg-blue-500 p-4 text-white text-center">
            {resultContent}
          </div>
        </Col>
        <Col span={24}>
          <div className="flex justify-center flex-wrap">
            <div className="w-full text-center my-4">
              <Image src={tableIcon}></Image>
            </div>
            <Row gutter={16} justify={'center'}>
              <Col>
                <Image src={pic1}></Image>
              </Col>
              <Col>
                <Image src={pic2}></Image>
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div className="text-center my-4">
            <Button type="primary">查看原文</Button>
            <div
              className={
                'absolute top-4 w-[98px] left-[50%] cursor-pointer h-[48px] overflow-hidden opacity-0'
              }
              style={{ left: 'calc(50%  - 49px)' }}
            >
              <Documents docList={[Reference]} />
            </div>
          </div>
          <div className="text-center my-2">
            *了解疾病的分期有利于精准治疗，患者朋友们不必过分担心，
            患者及家属只需按照医嘱积极进行相应的治疗，祝您早日康复！
          </div>

          <div className="mt-2">
            <div className="mb-2">参考文献</div>
            <p>
              1.中国医师协会血液科医师分会, 中华医学会血液学分会.
              中国多发性骨髓瘤诊治指南（2022年修订） [J] . 中华内科杂志, 2022,
              61(5) : 480-487.
            </p>
            <p>
              2.Rajkumar SV. Multiple myeloma: 2016 update on diagnosis,
              risk-stratification, and management. Am J Hematol.
              2016;91(7):719-734.
            </p>
            <p>
              3.Joanne L C Tan,et al.The second revision of the International
              Staging System (R2-ISS) stratifies progression-free and overall
              survival in multiple myeloma: real world data results is an
              australian and new zealand population.Br J Haematol . 2022 Nov 2.
              doi: 10.1111/bjh.18536.
            </p>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default Result;
