import { Navigate, } from '@umijs/max';

export default () => {
    return <Navigate to={'/home'} />;
};
