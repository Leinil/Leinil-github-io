import { Viewer, Worker } from '@react-pdf-viewer/core';
import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout';
import { Grid } from 'antd';

import Reference from '@/assets/references.pdf';

import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';

const PDFPreview = () => {
  const defaultLayoutPluginInstance = defaultLayoutPlugin();
  const { md } = Grid.useBreakpoint();

  return (
    <Worker workerUrl={Reference}>
      <div style={{ height: '750px' }}>
        <Viewer
          fileUrl={`${process.env.PUBLIC_URL}/pdf-open-parameters.pdf`}
          plugins={[defaultLayoutPluginInstance]}
        />
      </div>
    </Worker>
  );
};

export default PDFPreview;
