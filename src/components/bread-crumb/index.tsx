import type { BreadcrumbProps } from 'antd';
import { Breadcrumb as BreadcrumbAntd } from 'antd';
import { FC } from 'react';
import { Icon } from 'umi';

import './index.less';

const Separator = () => {
  return <Icon icon="local:arrow-right" className="flex items-center" />;
};

interface Props {
  items: BreadcrumbProps['items'];
}

 const BreadCrumb: FC<Props> = ({ items }) => {
  function itemRender(item: any, _: any, items: any) {
    const last = items.indexOf(item) === items.length - 1;
    return last ? (
      <span>{item.title}</span>
    ) : (
      <a onClick={() => typeof item.onClick === 'function' && item.onClick()}>
        {item.title}
      </a>
    );
  }

  return (
    <div className={'breadcrumbContainer'}>
      <BreadcrumbAntd
        items={items || []}
        itemRender={itemRender}
        separator={<Separator />}
      />
    </div>
  );
};

export default BreadCrumb
