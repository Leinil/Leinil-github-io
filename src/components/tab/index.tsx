import { useIntl } from '@umijs/max';
import { FC, PropsWithChildren } from 'react';
import { defineMessages } from 'react-intl';

type ListTabConfig = {
  label: any;
  status: string[];
};

interface TabProps {
  items: ListTabConfig[];
  value: string[];
  onClick: (value: any) => void;
}

const defaultValue = [
  {
    label: defineMessages({
      id: 'Covered',
    }),
    status: ['Issued', 'Protected'],
  },
  {
    label: defineMessages({
      id: 'Cancelled',
    }),
    // Canceled 是 policy的，Cancelled 是 Claim的。区别是有几个‘l’
    status: ['Canceled', 'Protected', 'Terminated'],
  },
  {
    label: defineMessages({
      id: 'Expired',
    }),
    status: ['Expired', 'End'],
  },
];

const Tab: FC<PropsWithChildren<TabProps>> = ({ items, onClick, value }) => {
  const { formatMessage } = useIntl();

  if (!items.length) return null;

  const { label } = items.find(
    ({ status }) => status.join(',') === value.join(','),
  ) || { label: defineMessages({ id: 'All' }) };

  return (
    <div className="flex gap-1">
      {items.map(({ label, status }, index) => (
        <div key={index} onClick={() => onClick(status)} className="">
          {formatMessage({ label })}
        </div>
      ))}
    </div>
  );
};

export default Tab;
